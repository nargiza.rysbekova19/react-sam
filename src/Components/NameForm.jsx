import React, { Component } from 'react';

class NameFrom extends React.Component {
    //constructor нужно для того чтобы мы смогли работать с состоянием(жизнь наешго компонента) компонента
    //он принимает в себе props это коллекция значений 
    //обязательно нужен constructor для работы с сотояниями(данными) компонента
    constructor(props) {
        //Есть только одна причина, когда нужно передавать props в super():
        //Когда вы хотите получить доступ к this.props в конструкторе.
        //props передаётся в компонент (служат как параметры функции), 
        super(props)

        // state — это обычные JavaScript-объекты. 
        // state находится внутри компонента (по аналогии с переменными, которые объявлены внутри функции).
        this.state = {
            // username = "",
            // password = "",
            text: ''
        };


        //Bind(this) нам нужен для того чтобы наша любая функция смогла получить дрступ к state(данным)
        //другими словами привязка функции к  state 
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }


    // обычная функциия пытается получить доступ к state 
    // но чтобы функция смогла изменить state нужно применить this.handleChange.bind(this); для привязки к данным(state)
    handleChange(event) {
        this.setState({ text: event.target.value });
        console.log(this.state.text);
    }

    handleSubmit(event) {
        alert('Отправленное имя: ' + this.state.text);
        event.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    Имя:
              <input type="text" value={this.state.value} onChange={this.handleChange} />
                </label>
                <input type="submit" value="Отправить" />
            </form>
        );
    }
}

export default NameFrom;