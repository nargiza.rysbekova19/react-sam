import React from 'react';
import classes from './Post.module.css';

const Post = (props) =>{

  // console.log(props.message)
    return <div >
      
      <div className={classes.item}>
        <img src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/close-up-of-tulips-blooming-in-field-royalty-free-image-1584131616.jpg?crop=0.630xw:1.00xh;0.186xw,0&resize=640:*" alt=""/>
        {props.message}
      </div>
     
    </div>
    
}

export default Post;