import React, { useEffect, useState } from 'react';
 function SignUp() {
    const [authData, setAuthData] = useState({ username: '', email: '', password: '' });


    const handleOnChange = (event) => {
        setAuthData({ ...authData, [event.target.name]: event.target.value });
    };

    const handleSubmit = async () => {
        console.log('signUp');
        let response = await fetch('url/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
            },
            body: JSON.stringify(authData),
        });

        let result = await response.json();
        alert(result.message);
    };

      useEffect(() => {
        console.log('authData', authData);
      }, [authData]);

    //  event

    return (
        <form style={{ display: 'flex', flexDirection: 'column', width: 300, alignItems: 'center' }}>
            <input placeholder='username' name='username' value={authData.username} onChange={handleOnChange} />
            <input placeholder='email' name='email' value={authData.email} onChange={handleOnChange} />
            <input
                placeholder='password'
                type='password'
                name='password'
                value={authData.password}
                onChange={handleOnChange}
            />
            <button type='button' onClick={handleSubmit}>
                SignUp
      </button>
        </form>
    );
}

export default SignUp