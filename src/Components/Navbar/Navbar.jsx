import React from 'react';
import classes from './Navbar.module.css';

const Nav = () => {
    return <nav className={classes.nav}>
        <div className='item'>
            <a href="/profile">Profile</a>
        </div>
        <div className='item'>
            <a href="/dialogs">Contacts</a>
        </div>
        <div className='item' >
            <a href="#">Brand</a>
        </div>
        <div className='item'>
            <a href="#">Adress</a>
        </div>
    </nav>
}

export default Nav;