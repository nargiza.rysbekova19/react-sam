import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import './App.css';
import Dialogs from './Components/Dialogs/Dialogs';
import Header from './Components/Header/Header';
// import NameFrom from './Components/NameForm';
// import SignUp from './Components/Sign-up';
import Nav from './Components/Navbar/Navbar';
import Profile from './Components/Profile/Profile';






const App = () =>{
  return (
    <BrowserRouter>
     <div className='app-wrapper'>
     <Header/>
     <Nav/>
    
     <div class = 'class.app-wrapper-content'>
       <Route path='/profile' component={Profile}/>
       <Route path='/dialogs' component={Dialogs}/>
        {/* <Profile/> */}
       {/* <Dialogs/> */}
     </div>
     
      {/* <NameFrom/>    */}
      {/* <SignUp/> */}

      

    </div>
    </BrowserRouter>
   
  );
}





export default App;
